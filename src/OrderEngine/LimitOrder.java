package OrderEngine;

public class LimitOrder {
    boolean isBid;
    int id;
    int price; // expressed in units of min-tick-size
    int qty;

    public LimitOrder(boolean isBid, int id, int price, int qty) {
        this.id = id;
        this.price = price;
        this.isBid = isBid;
        this.qty = qty;
    }
}
