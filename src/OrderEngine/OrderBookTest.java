package OrderEngine;

import org.junit.jupiter.api.Test;

import java.io.FileWriter;
import java.io.IOException;
import java.util.Date;
import java.util.LinkedList;
import java.util.Random;

import static org.junit.jupiter.api.Assertions.*;

class OrderBookTest {
    @Test
    void place_multiple_same_price() {
        OrderBook orderBook = new OrderBook(100, 5);
        orderBook.place(new LimitOrder(false, 0, 3, 1841418547));
        orderBook.place(new LimitOrder(true, 1, 1, 1138972289));
        orderBook.place(new LimitOrder(false, 2, 2, 1319542429));
        orderBook.place(new LimitOrder(true, 3, 2, 1022528117));
        orderBook.place(new LimitOrder(false, 4, 4, 1641017383));
        orderBook.place(new LimitOrder(true, 5, 3, 2057970082));
        orderBook.place(new LimitOrder(true, 6, 1, 1179054615));
        orderBook.place(new LimitOrder(true, 7, 3, 969661214));
        orderBook.place(new LimitOrder(false, 8, 3, 1471725159));
        orderBook.place(new LimitOrder(true, 9, 3, 1129527434));
        orderBook.place(new LimitOrder(true, 10, 3, 1262395332));
        orderBook.place(new LimitOrder(false, 11, 1, 2084929432));
    }

    @Test
    void place() {
        OrderBook orderBook = new OrderBook(100, 1000000);
        orderBook.place(new LimitOrder(true, 1, 100, 10));
        orderBook.place(new LimitOrder(true, 2, 100, 10));
        orderBook.place(new LimitOrder(true, 3, 95, 10));
        orderBook.place(new LimitOrder(false, 4, 99, 32));
        orderBook.place(new LimitOrder(false, 5, 92, 5));
        LinkedList<OrderMatch> matches = orderBook.matches;
        assertEquals(3, matches.size());
        assertEquals(new OrderMatch(10, 99, 1, 4, 100), matches.get(0));
        assertEquals(new OrderMatch(10, 99, 2, 4, 100), matches.get(1));
        assertEquals(new OrderMatch(5, 93, 3, 5, 100), matches.get(2));
    }

    @Test
    void performaceTest() throws IOException {
        int maxNumOrders = 1000000, startNumOrders = 0, stepSize = 50000, rounds = 5, maxPricePoints = 1000000;
        FileWriter reportFile = new FileWriter("report.tsv");
        reportFile.append("Orders\tMatches\tTime\n");
        for (int numOrders = startNumOrders; numOrders < maxNumOrders; numOrders += stepSize) {
            for (int round = 0; round < rounds; ++round) {
                OrderBook orderBook = new OrderBook(100, maxPricePoints);
                Random r = new Random();
                Date start = new Date();
                for (int i = 0; i < numOrders; ++i) {
                    orderBook.place(new LimitOrder(r.nextBoolean(), i, r.nextInt(maxPricePoints), r.nextInt()));
                }
                Date finish = new Date();
                long runningTime = (finish.getTime() - start.getTime());
                reportFile.append(String.valueOf(numOrders))
                        .append("\t")
                        .append(String.valueOf(orderBook.matches.size()))
                        .append("\t")
                        .append(String.valueOf(runningTime))
                        .append("\n");
                System.out.println("Processed " + numOrders + " random orders, " + orderBook.matches.size() + " matches in " +
                        runningTime + " milliseconds");
                System.out.println((((float) numOrders) / runningTime) * 1000 + " orders per second");
                System.out.println((((float) orderBook.matches.size()) / runningTime) * 1000 + " matches per second");
            }
        }
        reportFile.close();
    }
}