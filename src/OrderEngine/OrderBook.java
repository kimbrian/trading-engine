package OrderEngine;

import java.util.LinkedList;
import java.util.ListIterator;
import java.util.PriorityQueue;

public class OrderBook {
    private int maxPricePoints;
    private PricePoint[] orderBook;
    private PriorityQueue<Integer> bidPriceHeap; // max heap
    private PriorityQueue<Integer> askPriceHeap; // min heap
    public LinkedList<OrderMatch> matches = new LinkedList<>();
    private int bestBidPrice;
    private int bestAskPrice;
    // this represents the factor by which we should divide 'price' to express it in units of the currency
    public int tickFactor;

    class PricePoint {
        LinkedList<LimitOrder> orders = new LinkedList<>();
    }

    public OrderBook(int tickFactor, int maxPricePoints) {
        this.maxPricePoints = maxPricePoints;
        this.tickFactor = tickFactor;
        orderBook = new PricePoint[this.maxPricePoints];
        for (int i = 0; i < this.maxPricePoints; ++i) orderBook[i] = new PricePoint();
        bestAskPrice = this.maxPricePoints;
        bestBidPrice = 0;
        bidPriceHeap = new PriorityQueue<>((x, y) -> y - x);
        askPriceHeap = new PriorityQueue<>();
    }

    public boolean place(LimitOrder order) {
        if (order.price <= 0 || order.price >= maxPricePoints) {
            return false;
        }
        if (order.isBid) {
            processBid(order);
        } else {
            processAsk(order);
        }
        return true;
    }

    private void processBid(LimitOrder bid) {
        if (bid.price >= bestAskPrice) {
            while (bid.qty > 0 && bid.price >= bestAskPrice) {
                ListIterator<LimitOrder> itr = orderBook[bestAskPrice].orders.listIterator();
                matchOrders(bid, itr, bestAskPrice);
                if (orderBook[bestAskPrice].orders.size() == 0) {
                    // scan for new best ask
                    updateBestAsk();
                }
            }
        }
        if (bid.qty > 0) {
            orderBook[bid.price].orders.add(bid);
            bidPriceHeap.add(bid.price);
            bestBidPrice = Math.max(bestBidPrice, bid.price);
        }
    }

    private void processAsk(LimitOrder ask) {
        if (ask.price <= bestBidPrice) {
            while (ask.qty > 0 && ask.price <= bestBidPrice) {
                ListIterator<LimitOrder> itr = orderBook[bestBidPrice].orders.listIterator();
                matchOrders(ask, itr, bestBidPrice);
                if (orderBook[bestBidPrice].orders.size() == 0) {
                    // scan for new best bid
                    updateBestBid();
                }
            }
        }
        if (ask.qty > 0) {
            orderBook[ask.price].orders.add(ask);
            askPriceHeap.add(ask.price);
            bestAskPrice = Math.min(bestAskPrice, ask.price);
        }
    }

    private void matchOrders(LimitOrder incomingOrder, ListIterator<LimitOrder> itr, int bestMatchingPrice) {
        do {
            // this shouldn't fail, as there should be at least one order at this price point
            // otherwise something else has gone wrong
            LimitOrder existingOrder = itr.next();
            int matchedQty;
            if (incomingOrder.qty >= existingOrder.qty) {
                incomingOrder.qty -= existingOrder.qty;
                matchedQty = existingOrder.qty;
                itr.remove();
            } else {
                existingOrder.qty -= incomingOrder.qty;
                matchedQty = incomingOrder.qty;
                incomingOrder.qty = 0;
            }
            matches.add(
                    new OrderMatch(
                            matchedQty,
                            (incomingOrder.price + bestMatchingPrice) / 2,
                            existingOrder.id,
                            incomingOrder.id,
                            tickFactor
                    )
            );
        } while (itr.hasNext() && incomingOrder.qty > 0);
    }

    private void updateBestAsk() {
        // find the next best price -- we need to discount all the entries that are the same as the current best price
        // note that this assumes that no orders have been deleted. That will require a little extra work
        askPriceHeap.poll();
        Integer nextBestAsk = askPriceHeap.peek();
        while (nextBestAsk != null && nextBestAsk == bestAskPrice) {
            askPriceHeap.poll();
            nextBestAsk = askPriceHeap.peek();
        }
        bestAskPrice = nextBestAsk == null ? maxPricePoints : nextBestAsk;
    }

    private void updateBestBid() {
        // find the next best price -- we need to discount all the entries that are the same as the current best price
        // note that this assumes that no orders have been deleted. That will require a little extra work
        bidPriceHeap.poll();
        Integer nextBestBid = bidPriceHeap.peek();
        while (nextBestBid != null && nextBestBid == bestBidPrice) {
            bidPriceHeap.poll();
            nextBestBid = bidPriceHeap.peek();
        }
        bestBidPrice = nextBestBid == null ? -1 : nextBestBid;
    }
}
