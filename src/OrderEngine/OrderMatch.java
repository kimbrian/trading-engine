package OrderEngine;

public class OrderMatch {
    public int price;
    public int tickFactor;
    public int makerId;
    public int takerId;
    public int qty;

    OrderMatch(int qty, int price, int makerId, int takerId, int tickFactor) {
        this.qty = qty;
        this.price = price;
        this.makerId = makerId;
        this.takerId = takerId;
        this.tickFactor = tickFactor;
    }

    @Override
    public boolean equals(Object o) {
        if (o == this)
            return true;
        if (!(o instanceof OrderMatch))
            return false;
        OrderMatch om = (OrderMatch) o;
        return (qty == om.qty &&
                price == om.price &&
                makerId == om.makerId &&
                takerId == om.takerId &&
                tickFactor == om.tickFactor);
    }
}